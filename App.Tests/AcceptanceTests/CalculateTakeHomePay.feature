﻿Feature: Calculate the take home pay
	In order to avoid silly mistakes
	As a payroll user
	I want to see the take home pay summary with employee location ,amount gross, incoming tax, Universal Social Charge and Net amount.

Scenario: Calculate Gross amount calculation
	Given the employee is paid 10.00 euros per hour	
	When the employee works 40 hours
	Then Gross amount is 400.00 euros

Scenario: Calculate income tax applying 25% and 40% rates for employee located in Ireland
	Given the employee located in Ireland
	When the take home pay has a gross amount up to €1000
	Then income tax amount is 310.00

Scenario: Calculate income tax only with 25% rate for employee located in Ireland
	Given the employee located in Ireland
	When the take home pay has a gross amount up to €600
	Then income tax amount is 150.00

Scenario: Calculate Universal social charge only with one rate up to 7% employee located in Ireland
	Given the employee located in Ireland
	When the take home pay has a gross amount up to €500
	Then the universal social charge is €35

Scenario: Calculate Universal social charge with applying 7% and 8% rates for employee located in Ireland
	Given the employee located in Ireland
	When the take home pay has a gross amount up to €600
	Then the universal social charge is €43

Scenario: Calculate a compulsory pension contribution with a flat rate up to 4% for employee located in Ireland
	Given the employee located in Ireland
	When the take home pay has a gross amount up to €1000
	Then the compulsory pension contribution amount is €40 

Scenario: Calculate income tax  with a flat rate up to 25% for employee based in Italy
	Given the employee located in Italy
	When the take home pay has a gross amount up to €1000
	Then income tax amount is 250.00
	
Scenario: Calculate a compulsory pension contribution with a flat rate up to 9.19% for employee located in Italy
	Given the employee located in Italy
	When the take home pay has a gross amount up to €1000
	Then the compulsory pension contribution amount is €91.90

Scenario: Calculate income tax only with 25% rate for employee located in Germany
	Given the employee located in Germany
	When the take home pay has a gross amount up to €400
	Then income tax amount is 100.00

Scenario: Calculate income tax applying 25% and 32% rate for employee located in Germany
	Given the employee located in Germany
	When the take home pay has a gross amount up to €500
	Then income tax amount is 132.00

Scenario: Calculate a compulsory pension contribution with a flat rate up to 2% for employee located in Germany
	Given the employee located in Germany
	When the take home pay has a gross amount up to €2000
	Then the compulsory pension contribution amount is €40

Scenario: Calculate Net amount for a employee based in Ireland
	Given the employee located in Ireland
	When the take home pay has a gross amount up to €600
	Then the Net amount is €383

Scenario:  Retrieve employee location name description from the take home pay
	Given the employee located in Ireland
	When  Get location name description from the take home pay
	Then  it will be Ireland