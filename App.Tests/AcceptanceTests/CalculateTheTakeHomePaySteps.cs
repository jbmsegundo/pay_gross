﻿using NUnit.Framework;
using Ryanair.App.Entities;
using Ryanair.App.Entities.Deductions;
using Ryanair.App.Factories;
using Ryanair.App.Tests.Builders;
using TechTalk.SpecFlow;

namespace Ryanair.App.Tests.AcceptanceTests
{
    [Binding]
    public class CalculateTheTakeHomePaySteps
    {
        private readonly EmployeeBuilder _employeeBuilder;
        private Employee _employee;
        private TakeHomePay _takeHomePay;

        public CalculateTheTakeHomePaySteps()
        {
            _employeeBuilder = new EmployeeBuilder();
        }

        [Given(@"the employee is paid (.*) euros per hour")]
        public void GivenTheEmployeeIsPaidEurosPerHour(decimal salaryPerHour)
        {
            _employeeBuilder.WithSalaryPerHour(salaryPerHour);
            _employee = _employeeBuilder.Build();
        }

        [When(@"the employee works (.*) hours")]
        public void WhenTheEmployeeWorksHours(decimal workedHours)
        {
            _employee.AddWorkedHours(workedHours);
        }

        [Then(@"Gross amount is (.*) euros")]
        public void ThenGrossAmountIsEuros(decimal grossAmount)
        {
            var takeHomePay = new TakeHomePay(_employee, new IrelandDeductionsCalculationsRules());
            Assert.AreEqual(grossAmount, takeHomePay.GrossAmount);
        }

        [Given(@"the employee located in (.*)")]
        public void GivenTheEmployeeLocatedIn(Location location)
        {
            _employeeBuilder.WithLocation(location);
            _employee = _employeeBuilder.Build();
        }

        [Given(@"hourlySalary (.*)")]
        public void GivenHourlySalary(decimal salary)
        {
            _employeeBuilder.WithSalaryPerHour(salary);
            _employee = _employeeBuilder.Build();
        }

        [Then(@"income tax amount is (.*)")]
        public void ThenIncomingTaxIs(decimal expectedIncomingTax)
        {
            Assert.AreEqual(expectedIncomingTax, _takeHomePay.IncomeTax);
        }

        [When(@"the take home pay has a gross amount up to €(.*)")]
        public void GivenTheEmployeeHasAGrossAmoutUpTo(int grossAmout)
        {
            var salaryPerHour = 10.0m;
            _employeeBuilder.WithSalaryPerHour(salaryPerHour);
            _employee = _employeeBuilder.Build();
            _employee.AddWorkedHours(grossAmout / salaryPerHour);
            _takeHomePay = new TakeHomePay(_employee, DeductionCalculationRulesFactory.CreateCalculationsRules(_employee.Location));
        }

        [Then(@"the universal social charge is €(.*)")]
        public void ThenTheUniversalSocialChargeIs(int usChargeAmount)
        {
            Assert.AreEqual(usChargeAmount, _takeHomePay.UniversalSocialCharge);
        }

        [Then(@"the compulsory pension contribution amount is €(.*)")]
        public void ThenTheCompulsoryPensionContributionAmountIs(decimal cpcAmount)
        {
            Assert.AreEqual(cpcAmount, _takeHomePay.CompulsoryPensionContribution);

        }
        [Then(@"the Net amount is €(.*)")]
        public void ThenTheNetAmountIs(decimal netAmount)
        {
            Assert.AreEqual(netAmount, _takeHomePay.NetAmount);
        }
        [When(@"Get location name description from the take home pay")]
        public void WhenGetLocationNameDescriptionFromTheTakeHomePay()
        {
            _takeHomePay = new TakeHomePay(_employee, DeductionCalculationRulesFactory.CreateCalculationsRules(_employee.Location));
        }


        [Then(@"it will be (.*)")]
        public void ThenTheLocationNameDescriptionInTheTakeHomePayIsIreland(string location)
        {
            Assert.AreEqual(location, _takeHomePay.EmployeeLocationDescription);
        }






    }
}
