﻿using System;
using System.Text;
using Moq;
using NUnit.Framework;
using Ryanair.App.Entities;

namespace Ryanair.App.Tests.UnitTests
{
    [TestFixture]
    public class ConsoleApplicationTests
    {
        private readonly IConsoleApplication _app;

        public ConsoleApplicationTests()
        {
            _app = new ConsoleApplication();
        }
        [Test]
        public void GivenTheNegativeValueForHourlyRate_WhenSave_ShouldTryException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _app.SaveHourlyRate(-1));
        }

        [Test]
        public void GivenThePositiveValueForHourlyRate_ShouldSaveTheValue()
        {
            _app.SaveHourlyRate(1);
            Assert.AreEqual(1,_app.HourlyRate);
        }

        [Test]
        public void GivenTheNegativeValueForWorkedHours_WhenSave_ShouldTryException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _app.SaveWorkedHours(-1));
        }

        [Test]
        public void GivenThePositiveValueFoWorkedHours_ShouldSaveTheValue()
        {
            _app.SaveWorkedHours(1);
            Assert.AreEqual(1, _app.WorkedHours);
        }

        [Test]
        public void GivenTheInvalidLocation_WhenSave_ShouldTryException()
        {
            Assert.Throws<ArgumentException>(() => _app.SaveLocation("Brazil"));
        }

        [Test]
        public void GivenTheValidLocation_WhenSave_ShouldSaveTheValue()
        {
            _app.SaveLocation("Ireland");
            Assert.AreEqual(Location.Ireland, _app.Location);
        }

        [Test]
        public void GivenTheNullValue_WhenSave_ShouldTryException()
        {
            Assert.Throws<ArgumentNullException>(() => _app.SaveLocation(null), "The should not be empty or null");
        }

        [Test]
        public void GivenTheTakeHomePay_ShouldBuildStringOutput()
        {
            var takeHomePayMock = new Mock<ITakeHomePay>();

            var fakeLocation = "Ireland";
            var fakeGrossAmount = 400;
            var fakeIncomeTax = 150;
            var fakedUSCharge = 140;
            var fakePension = 100;
            var fakeNetAmount = 10;

            takeHomePayMock.SetupGet(x => x.GrossAmount).Returns(fakeGrossAmount);
            takeHomePayMock.SetupGet(x => x.IncomeTax).Returns(fakeIncomeTax);
            takeHomePayMock.SetupGet(x => x.UniversalSocialCharge).Returns(fakedUSCharge);
            takeHomePayMock.SetupGet(x => x.CompulsoryPensionContribution).Returns(fakePension);
            takeHomePayMock.SetupGet(x => x.EmployeeLocationDescription).Returns(fakeLocation);
            takeHomePayMock.SetupGet(x => x.NetAmount).Returns(fakeNetAmount);

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine($"Employee location: Ireland");
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine($"Gross Amount: {fakeGrossAmount:c2}");
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("Less deductions");
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine($"Income Tax: {fakeIncomeTax:c2}");
            stringBuilder.AppendLine($"Universal Social Charge: {fakedUSCharge:c2}");
            stringBuilder.AppendLine($"Pension: {fakePension:c2}");
            stringBuilder.AppendLine($"Net Amount: {fakeNetAmount:c2}");

            string expectedOutput = stringBuilder.ToString();
            string output = _app.BuildOutput(takeHomePayMock.Object);
            Assert.AreEqual(expectedOutput.Length, output.Length);
            Assert.AreEqual(expectedOutput.ToString(), output);

        }

    }
}
