﻿using Ryanair.App.Entities;

namespace Ryanair.App.Tests.Builders
{
    public class EmployeeBuilder
    {
        private Location _location;
        private decimal _salaryPerHour;
      
        public EmployeeBuilder()
        {
            _location = Location.Ireland;
            _salaryPerHour = 10;
        }

        public Employee Build()
        {
            var employee = new Employee(_location, _salaryPerHour);
            return employee;
        }

        public EmployeeBuilder WithLocation(Location location)
        {
            _location = location;
            return this;
        }

        public EmployeeBuilder WithSalaryPerHour(decimal salaryPerHour)
        {
            _salaryPerHour = salaryPerHour;
            return this;
        }
    }
}
