﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Ryanair.App.Entities;

namespace Ryanair.App
{
    class Program
    {
        private static readonly List<string> AvailableLocations = Enum.GetNames(typeof(Location)).ToList();

        static void Main(string[] args)
        {
            IConsoleApplication app = new ConsoleApplication();

            Console.WriteLine("Please enter the hours worked: ");
            var workedHours = GetDecimalFromStdIn();
            app.SaveWorkedHours(workedHours);

            Console.WriteLine("Please enter the hourly rate: : ");
            var hourlyRate = GetDecimalFromStdIn();
            app.SaveHourlyRate(hourlyRate);

            Console.WriteLine($"Please enter the employee’s location: ({string.Join(",", AvailableLocations)})");
            var location = GetLocation();
            app.SaveLocation(location);

            var takeHomePay = CreateTheTakeHomePay(app);
            var summaryOutput = app.BuildOutput(takeHomePay);
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine(summaryOutput);

        }

        private static TakeHomePay CreateTheTakeHomePay(IConsoleApplication app)
        {
            var employee = new Employee(app.Location, app.HourlyRate);
            employee.AddWorkedHours(app.WorkedHours);
            var takeHomePay = new TakeHomePay(employee,
                Factories.DeductionCalculationRulesFactory.CreateCalculationsRules(app.Location));
            return takeHomePay;
        }

        private static string GetLocation()
        {
            string location = Console.ReadLine();
            while (!AvailableLocations.Any(locationListItem=> string.Equals(locationListItem, location, StringComparison.OrdinalIgnoreCase)))
            {
                Console.WriteLine($"Invalid location! The available locations are:{string.Join(",", AvailableLocations)}  ");
                location = Console.ReadLine();
            }

            return location;
        }

        private static decimal GetDecimalFromStdIn()
        {
            decimal decimalValue;
            while (!decimal.TryParse(Console.ReadLine(), out decimalValue))
            {
                Console.WriteLine("Invalid input! Please, enter a numerical value greater than 0");
            }

            return decimalValue;
        }
        
    }
}
