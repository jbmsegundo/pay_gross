﻿using Ryanair.App.Entities;

namespace Ryanair.App
{
    public interface IConsoleApplication
    {
        void SaveHourlyRate(decimal hourlyRate);
        decimal HourlyRate { get; }
        decimal WorkedHours { get; }
        Location Location { get; }
        void SaveWorkedHours(decimal workedHours);
        void SaveLocation(string saoPaulo);
        string BuildOutput(ITakeHomePay takeHomePay);
    }
}