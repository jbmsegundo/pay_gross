﻿using System;
using System.Text;
using Ryanair.App.Entities;

namespace Ryanair.App
{
    public class ConsoleApplication : IConsoleApplication
    {
        public decimal HourlyRate { get; private set; }
        public decimal WorkedHours { get; private set; }
        public Location Location { get; private set; }

        public void SaveHourlyRate(decimal hourlyRate)
        {
            PositiveValueValidation(hourlyRate);
            HourlyRate = hourlyRate;
        }

        public void SaveWorkedHours(decimal workedHours)
        {
            PositiveValueValidation(workedHours);
            WorkedHours = workedHours;
        }

        public void SaveLocation(string locationName)
        {
            if (string.IsNullOrWhiteSpace(locationName))
                throw new ArgumentNullException(nameof(locationName), "The should not be empty or null");
            Location location;
            if (!Enum.TryParse(locationName.Trim(), true, out location))
                throw new ArgumentException("Invalid location");
            this.Location = location;

        }

        public string BuildOutput(ITakeHomePay takeHomePay)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine($"Employee location: {takeHomePay.EmployeeLocationDescription}");
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine($"Gross Amount: {takeHomePay.GrossAmount:C2}");
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("Less deductions");
            stringBuilder.AppendLine("");
            stringBuilder.AppendLine($"Income Tax: {takeHomePay.IncomeTax:C2}");
            stringBuilder.AppendLine($"Universal Social Charge: {takeHomePay.UniversalSocialCharge:C2}");
            stringBuilder.AppendLine($"Pension: {takeHomePay.CompulsoryPensionContribution:C2}");
            stringBuilder.AppendLine($"Net Amount: {takeHomePay.NetAmount:C2}");
            
            return stringBuilder.ToString();
        }

        private static void PositiveValueValidation(decimal value)
        {
            if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value), "The value should be positive");
        }

    }
}
