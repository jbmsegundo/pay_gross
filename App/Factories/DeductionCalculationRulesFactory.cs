﻿using Ryanair.App.Entities;
using Ryanair.App.Entities.Deductions;

namespace Ryanair.App.Factories
{
    public static class DeductionCalculationRulesFactory
    {
        public static IDeductionCalculationsRules CreateCalculationsRules(Location location)
        {
            switch (location)
            {
                case Location.Ireland:
                    return new IrelandDeductionsCalculationsRules();
                case Location.Italy:
                    return new ItalyDeductionsCalculationsRules();
                case Location.Germany:
                    return new GermanyDeductionsCalculationsRules();
                default:
                    return new IrelandDeductionsCalculationsRules();
            }
        }
    }
}
