﻿namespace Ryanair.App.Entities
{
    public interface ITakeHomePay
    {
        decimal CompulsoryPensionContribution { get; }
        decimal GrossAmount { get; }
        decimal IncomeTax { get; }
        decimal UniversalSocialCharge { get; }
        decimal NetAmount { get; }
        string EmployeeLocationDescription { get;}
    }
}