﻿using Ryanair.App.Entities.Deductions;

namespace Ryanair.App.Entities
{
    public class TakeHomePay : ITakeHomePay
    {
        private readonly Employee _employee;
        private readonly IDeductionCalculationsRules _incomeTaxDeductionCalculationsRules;

        public decimal GrossAmount => _employee.CurrentWorkedHours * _employee.SalaryPerHour;

        public decimal IncomeTax => _incomeTaxDeductionCalculationsRules.GetDeductionValue(Deduction.IncomeTax, GrossAmount);

        public decimal UniversalSocialCharge => _incomeTaxDeductionCalculationsRules.GetDeductionValue(Deduction.UniversalSocialCharge, GrossAmount);

        public decimal CompulsoryPensionContribution
            => _incomeTaxDeductionCalculationsRules.GetDeductionValue(Deduction.CompulsoryPensionContribution, GrossAmount);

        public decimal NetAmount => GrossAmount - IncomeTax - CompulsoryPensionContribution - UniversalSocialCharge;
        public string EmployeeLocationDescription => _employee.Location.ToString();

        public TakeHomePay(Employee employee, IDeductionCalculationsRules incomeTaxDeductionCalculationsRules)
        {
            _employee = employee;
            _incomeTaxDeductionCalculationsRules = incomeTaxDeductionCalculationsRules;
            
        }
        

       
    }
    
}
