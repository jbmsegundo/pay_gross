﻿namespace Ryanair.App.Entities.Deductions
{
    public class ItalyDeductionsCalculationsRules : DeductionsCalculationRulesBase, IDeductionCalculationsRules
    {
        public ItalyDeductionsCalculationsRules()
        {
            MapCalculationsRules();
        }
        private void MapCalculationsRules()
        {
            Calculations.Add(Deduction.IncomeTax, GetIncomeTax);
            Calculations.Add(Deduction.CompulsoryPensionContribution, GetInpsAmount);
        }
        public decimal GetIncomeTax(decimal grossAmount)
        {
            return grossAmount*0.25m;
        }

        public decimal GetInpsAmount(decimal grossAmount)
        {
            return grossAmount*0.0919m;
        }
        
    }
}
