﻿namespace Ryanair.App.Entities.Deductions
{
    public interface IDeductionCalculationsRules
    {
        decimal GetDeductionValue(Deduction deduction, decimal grossAmount);
    }

    public enum Deduction
    {
        IncomeTax,
        CompulsoryPensionContribution,
        UniversalSocialCharge
    }
}
