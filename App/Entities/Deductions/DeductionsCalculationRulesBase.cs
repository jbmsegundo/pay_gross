﻿using System;
using System.Collections.Generic;

namespace Ryanair.App.Entities.Deductions
{
    public abstract class DeductionsCalculationRulesBase
    {
        protected Dictionary<Deduction, Func<decimal, decimal>> Calculations;

        public decimal GetDeductionValue(Deduction deduction, decimal grossAmount)
        {
            Func<decimal, decimal> deductionRule;
            Calculations.TryGetValue(deduction, out deductionRule);
            return deductionRule?.Invoke(grossAmount) ?? 0;
        }
        protected DeductionsCalculationRulesBase()
        {
            Calculations = new Dictionary<Deduction, Func<decimal, decimal>>();
        }
    }
}
