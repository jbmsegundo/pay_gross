﻿namespace Ryanair.App.Entities.Deductions
{
    public class GermanyDeductionsCalculationsRules : DeductionsCalculationRulesBase, IDeductionCalculationsRules
    {
        public GermanyDeductionsCalculationsRules()
        {
            MapCalculationsRules();   
        }

        private void MapCalculationsRules()
        {
            Calculations.Add(Deduction.IncomeTax, GetIncomeTax);
            Calculations.Add(Deduction.CompulsoryPensionContribution, GetCompulsoryPensionContribution);
        }
        public decimal GetIncomeTax(decimal grossAmount)
        {
            if (grossAmount <= 400)
                return 0.25m * grossAmount;
            var incomingTax = 0.25m * 400;
            var secondLevel = grossAmount - 400;
            incomingTax += 0.32m * secondLevel;
            return incomingTax;
        }
        public decimal GetCompulsoryPensionContribution(decimal grossAmount)
        {
            return grossAmount * 0.02m;
        }
    }
}
