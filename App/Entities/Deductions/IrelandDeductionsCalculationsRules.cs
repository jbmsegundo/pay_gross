﻿namespace Ryanair.App.Entities.Deductions
{
    public class IrelandDeductionsCalculationsRules : DeductionsCalculationRulesBase, IDeductionCalculationsRules 
    {
        public IrelandDeductionsCalculationsRules()
        {
            MapCalculationsRules();
        }
        private void MapCalculationsRules()
        {
            Calculations.Add(Deduction.IncomeTax, GetIncomeTax);
            Calculations.Add(Deduction.UniversalSocialCharge, GetUniversalCharge);
            Calculations.Add(Deduction.CompulsoryPensionContribution, GetCompulsoryPensionContribution);
        }

        public decimal GetIncomeTax(decimal grossAmount)
        {
            if (grossAmount <= 600)
                return 0.25m * grossAmount;
            var incomingTax = 0.25m * 600;
            var secondLevel = grossAmount - 600;
            incomingTax += 0.40m * secondLevel;
            return incomingTax;

        }

        public decimal GetUniversalCharge(decimal grossAmount)
        {
            var baseRate = 0.07m;
            if (grossAmount <= 500)
                return baseRate * grossAmount;
            var universalCharge = baseRate * 500;
            var secondLevel = grossAmount - 500;
            universalCharge += 0.08m * secondLevel;
            return universalCharge;
        }

        public decimal GetCompulsoryPensionContribution(decimal grossAmount)
        {
            return grossAmount*0.04m;
        }

    }
}