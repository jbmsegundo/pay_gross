﻿namespace Ryanair.App.Entities
{
    public class Employee
    {
        public decimal SalaryPerHour { get; private set; }
        public decimal CurrentWorkedHours { get; private set; }
        public Location Location { get; private set; }

        public Employee(Location location, decimal salaryPerHour)
        {
            SalaryPerHour = salaryPerHour;
            Location = location;
        }

        public void AddWorkedHours(decimal workedHours)
        {
            CurrentWorkedHours += workedHours;
        }

       
    }

    public enum Location
    {
        Ireland,
        Italy,
        Germany,
    }
}